using UnityEngine;

public class Launcher : MonoBehaviour
{
    [SerializeField] GameObject _projectilePrefab;
    [SerializeField] float _fireDelay = 0.25f;
    [SerializeField] KeyCode _fireKey = KeyCode.Space;

    float _fireTime;

    bool CanFire => Time.time >= _fireTime;

    void OnEnable()
    {
        _fireTime = Time.time + _fireDelay;
    }

    void Update()
    {
        if (!CanFire) return;

        if (Input.GetKeyDown(_fireKey))
        {
            FireProjectile();
        }
    }

    void FireProjectile()
    {
        _fireTime = Time.time + _fireDelay;
        Instantiate(_projectilePrefab, transform.position, Quaternion.identity);
    }
}