using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] float _speed, _duration = 3f;
    
    Transform _transform;
    float _endTime;

    void Awake()
    {
        _transform = transform;
    }

    void OnEnable()
    {
        _endTime = Time.time + _duration;
    }

    void Update()
    {
        if (Time.time >= _endTime)
        {
            Destroy(gameObject);
            return;
        }
        _transform.position += _transform.right * (_speed * Time.deltaTime);
    }
}
