using UnityEngine;

public class InputConfiguration : MonoBehaviour
{
    [SerializeField] KeyCode _forward = KeyCode.D;
    [SerializeField] KeyCode _back = KeyCode.A;
    [SerializeField] KeyCode _up = KeyCode.W; 
    [SerializeField] KeyCode _down = KeyCode.S;

    public KeyCode Forward => _forward;
    public KeyCode Backwards => _back;
    public KeyCode Up => _up;
    public KeyCode Down => _down;
}
