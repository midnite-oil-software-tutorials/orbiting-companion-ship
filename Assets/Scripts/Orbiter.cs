using UnityEngine;

public class Orbiter : MonoBehaviour
{
    [SerializeField] float _moveLimit = 1.5f, _moveSpeed = 15;
    InputConfiguration _inputCfg;

    bool ShouldMoveDown => Input.GetKey(_inputCfg.Up) && transform.localPosition.y > -_moveLimit;
    bool ShouldMoveUp => Input.GetKey(_inputCfg.Down) && transform.localPosition.y < _moveLimit;
    bool ShouldMoveBackwards => Input.GetKey(_inputCfg.Forward) && transform.localPosition.x > -_moveLimit;
    bool ShouldMoveForward => Input.GetKey(_inputCfg.Backwards) && transform.localPosition.x < _moveLimit;
    void Awake()
    {
        _inputCfg = FindObjectOfType<InputConfiguration>(true);
    }

    void Update()
    {
        HandleForwardMovement();
        HandleBackwardMovement();
        HandleUpMovement();
        HandleDownMovement();
    }

    void HandleForwardMovement()
    {
        if (ShouldMoveForward)
        {
            MoveForward();
        }
    }

    void MoveForward()
    {
        transform.localPosition += Vector3.right * (_moveSpeed * Time.deltaTime);
    }

    void HandleBackwardMovement()
    {
        if (ShouldMoveBackwards)
        {
            MoveBackwards();
        }
    }

    void MoveBackwards()
    {
        transform.localPosition += Vector3.left * (_moveSpeed * Time.deltaTime);
    }

    void HandleUpMovement()
    {
        if (ShouldMoveUp)
        {
            MoveUp();
        }
    }

    void MoveUp()
    {
        transform.localPosition += Vector3.up * (_moveSpeed * Time.deltaTime);
    }

    void HandleDownMovement()
    {
        if (ShouldMoveDown)
        {
            MoveDown();
        }
    }

    void MoveDown()
    {
        transform.localPosition += Vector3.down * (_moveSpeed * Time.deltaTime);
    }
}
