﻿using UnityEngine;

public class ScrollingBackground : MonoBehaviour
{
    [SerializeField]
    float _scrollSpeed = 2f;

    Renderer _renderer;
    static readonly int MainTex = Shader.PropertyToID("_MainTex");

    void Awake()
    {
        _renderer = GetComponent<Renderer>();
    }

    void Update()
    {
        Vector2 currentTextureOffset = _renderer.material.GetTextureOffset(MainTex);
        float distanceToScroll = Time.deltaTime * _scrollSpeed;
        float newXOffset = currentTextureOffset.x + distanceToScroll;
        Vector2 newOffset = new Vector2(newXOffset, currentTextureOffset.y);
        _renderer.material.SetTextureOffset(MainTex, newOffset);
    }
}
