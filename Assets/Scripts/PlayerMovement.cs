using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] Vector3 _startPosition;
    [SerializeField] float _moveLimit = 2f, _moveSpeed = 5f;

    Transform _transform;
    InputConfiguration _inputCfg;

    bool CanMoveUp => Input.GetKey(_inputCfg.Up) && _transform.position.y < (_startPosition.y + _moveLimit);
    bool CanMoveDown => Input.GetKey(_inputCfg.Down) && _transform.position.y > (_startPosition.y - _moveLimit);
    bool CanMoveForwards => Input.GetKey(_inputCfg.Forward) && _transform.position.x < (_startPosition.x + _moveLimit);
    bool CanMoveBackwards => Input.GetKey(_inputCfg.Backwards) && _transform.position.x > (_startPosition.x - _moveLimit);

    void Awake()
    {
        _transform = transform;
        _inputCfg = FindObjectOfType<InputConfiguration>(true);
    }

    void Update()
    {
        HandleForwardMovement();
        HandleBackwardMovement();
        HandleUpMovement();
        HandleDownMovement();
    }

    void HandleUpMovement()
    {
        if (CanMoveUp)
        {
            MoveUp();
        }
    }

    void MoveUp()
    {
        _transform.position += Vector3.up * (_moveSpeed * Time.deltaTime);
    }

    void HandleDownMovement()
    {
        if (CanMoveDown)
        {
            MoveDown();
        }
    }

    void MoveDown()
    {
        _transform.position += Vector3.down * (_moveSpeed * Time.deltaTime);
    }

    void HandleForwardMovement()
    {
        if (CanMoveForwards)
        {
            MoveForwards();
        }
    }

    void MoveForwards()
    {
        _transform.position += Vector3.right * (_moveSpeed * Time.deltaTime);
    }

    void HandleBackwardMovement()
    {
        if (CanMoveBackwards)
        {
            MoveBackwards();
        }
    }

    void MoveBackwards()
    {
        _transform.position += Vector3.left * (_moveSpeed * Time.deltaTime);
    }
}
