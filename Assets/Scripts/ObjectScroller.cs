using System;
using UnityEngine;

public class ObjectScroller : MonoBehaviour
{
    [SerializeField] float _moveSpeed = 6f;
    
    Transform _transform;

    void Awake()
    {
        _transform = transform;
    }

    void Update()
    {
        _transform.position += Vector3.left * (_moveSpeed * Time.deltaTime);
    }
}
